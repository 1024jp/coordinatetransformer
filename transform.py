#!/usr/bin/env python

"""Commandline interface of transformer.py.

Rotate and translate coodinate in a csv file.
"""

import csv

from modules import argsparser, transformer


__date__ = '2013-04-16'
__author__ = '1024jp <http://wolfrosch.com/>'


def transform_csv(transformer, input_file, output_file, delimiter=',',
                  cols=(1, 2)):
    """Transform coodinates x,y in csv file and write them in a new file.

    arguments:
    transformer -- Transformer instance.
    input_file -- file object of source file with a read permission.
    output_file -- file object to write result data with a write permission.
    delimiter -- delimiter of csv file.
    cols -- column numbers of x, y data in the csv file. (number starts with 1)
    """
    x_index = cols[0] - 1
    y_index = cols[1] - 1

    with input_file, output_file:
        reader = csv.reader(input_file, delimiter=delimiter)
        writer = csv.writer(output_file, delimiter=delimiter,
                            lineterminator='\n')

        # check header
        has_header = csv.Sniffer().has_header(input_file.readline())
        input_file.seek(0)
        if has_header:
            header = next(reader)
            writer.writerow(header)

        for row in reader:
            x = float(row[x_index])
            y = float(row[y_index])

            # transform
            x, y = transformer.transform(x, y)

            # rewrite row
            row[x_index] = int(x)
            row[y_index] = int(y)

            # write
            writer.writerow(row)


if __name__ == "__main__":
    # parse command-line options
    args = argsparser.parse()

    # create transformer instance
    transformer = transformer.Transformer(x=args.x, y=args.y, r=args.rotate,
                                          cx=args.origin[0], cy=args.origin[1])

    # execute
    transform_csv(transformer, args.file, args.out, delimiter=args.delimiter,
                  cols=args.cols)
