#!/usr/bin/env python

"""Rotate and move inputted coordinates.
"""

import math

__date__ = '2013-04-26'
__author__ = '1024jp <http://wolfrosch.com/>'


class Point:
    """General x, y coordinate set."""

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def move(self, x=0, y=0):
        """Move coordinate."""
        self.x += x
        self.y += y

    def rotate(self, angle, x=0, y=0, degree=True):
        """Rotate coordinate around the origin(x, y).

        Default unit of the angle argument is degree.
        Turn degree argument to False, if the angle value is in radian.
        """
        if degree:
            angle = math.radians(angle)

        new_x = (self.x - x) * math.cos(angle) - (self.y - y) * math.sin(angle)
        new_y = (self.x - x) * math.sin(angle) + (self.y - y) * math.cos(angle)

        self.x = new_x
        self.y = new_y


class Transformer:
    """Converter to transform x, y coodinates with set values."""

    def __init__(self, x=0, y=0, r=0, cx=0, cy=0, degree=True):
        self.x = x
        self.y = y
        self.r = r
        self.cx = cx
        self.cy = cy
        self.degree = degree

    def transform(self, x, y):
        point = Point(x, y)
        if self.r:
            point.rotate(self.r, x=self.cx, y=self.cy, degree=self.degree)
        point.move(self.x, self.y)

        return point.x, point.y


def test():
    transformer = Transformer(r=45, x=2.5, y=1)

    print([round(c, 3) for c in transformer.transform(1, 1)])


if __name__ == "__main__":
    test()
