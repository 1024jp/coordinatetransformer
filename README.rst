========================
CoordinateTransformer
========================

CSVファイル内のx, y座標データを一括で回転・移動させるコマンドライン・スクリプトです。
modules/transformer.py単体でPythonのモジュールとしても使えます。


******************
環境要件
******************

Python 2.x or 3.x


******************
使い方
******************

Terminalから任意の引数を付けて実行します。

取りうる引数について詳しくは、::

    $ ./transform.py --help

で表示されるヘルプをご覧下さい。

例1
============
::

    $ ./transform.py source.csv --out result.csv --delimiter tab --cols 2 3 --rotate 90 --origin 50 0

この場合、source.csv を TSV (tab-separated values) ファイルとして解釈し、ファイル内の 2, 3列目にあるx, yデータを 座標(50, 0) を中心に90度回転させたものを result.csv に書き出すことになります。

例2
============
::

    $ ./transform.py source.txt --tracklog -x 1000

source.csv が tracklog ファイル形式であると解釈し、各x座標を 1000 移動した値を new_source.txt (デフォルトの出力先) に書き出します。


******************
クレジット
******************
Author: 1024jp